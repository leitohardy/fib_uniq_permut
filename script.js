window.onload = function() {

    function fib(n) {
        let result = [];
        let prev = 0;

        for (let i = 0; i <= n; i++) {
        
            if (i > 1) {
                prev = result[i-1] + result[i-2];
                result.push(prev)
            } else {
                result.push(i)
            }
           
        }
        return result[result.length - 1];
    }

    function uniq(arr) {
        let uniqArr = new Map();
        let result = [];

        for (let item of arr) {
            uniqArr.set(item, item);
        }

        uniqArr.forEach(value => result.push(value));

        return result;
    }

    function permut(string) {
        if (string.length < 2) return string;

        var permutations = [];
        for (var i = 0; i < string.length; i++) {
            var char = string[i];
            if (string.indexOf(char) != i) {
                continue;
            }
        
            var remainingString = string.slice(0, i) + string.slice(i + 1, string.length); 

            for (var subPermutation of permut(remainingString)) {
                permutations.push(char + subPermutation)
            }
        }

        return permutations;
    }

    let modal = {

        init: function() {
            this.modalWrapper = document.querySelector('.modal-wrapper');
            this.modalElement = document.querySelector('#modal');
            this.closeBtn = document.querySelector('.close');
            this.executeBtn = document.querySelector('#execute');

            this.modalDesc = document.querySelector('.task-desc');
            this.paramsExample= document.querySelector('.params');
            this.modalContent = document.querySelector('#content');

            this.paramData;
            this.selectedFunc;

            this.show();

            this.closeBtn.addEventListener('click', () => {
                this.hide();
                this.clear();
            })

            this.executeBtn.addEventListener('click', () => {
                this.getDataFromInput();
                this.executeFunc(this.selectedFunc, this.paramData);
            })


        },

        show: function() {
            this.modalElement.style.display = 'block';
        },

        hide: function() {
            this.modalElement.style.display = 'none';
            this.modalWrapper.classList.add("hidden");
        },

        open: function(type) {
            this.show();
            this.modalWrapper.classList.remove("hidden");

            switch (type) {
                case 'task1':
                    this.changeDesc(
                        {
                            desc: `Реализуйте функцию ​fib(), возвращающую n-ное число Фибоначчи. `,
                            paramExample: `Введите любое число. Например: 7`
                        }
                    );

                    this.selectedFunc = fib;
                  break;
                  
                case 'task2':
                    this.changeDesc(
                        {
                            desc: `Реализуйте функцию ​uniq(), которая принимает массив чисел и возвращает уникальные числа, найденные в нём.`,
                            paramExample: `Введите любые числа через запятую. Например: 7, 2, 3, 2`
                        }
                    );
                    
                    this.selectedFunc = uniq;
                  break;

                case 'task3':
                    this.changeDesc(
                        {
                            desc: `Реализуйте функцию ​permute(), которая возвращает массив строк, содержащий все пермутации заданной строки`,
                            paramExample: `Введите любую строку. Например: 'abc'`
                        }
                    );
                    
                    this.selectedFunc = permut;
                    break;

                default:
                    this.hide();
                    throw('Incorrect modal type!');
                    break;
            }

        },

        changeDesc: function(htmlObj) {
            this.modalDesc.innerHTML = htmlObj.desc;
            this.paramsExample.innerHTML = htmlObj.paramExample;
        },

        changeOutputContent: function(data) {

            let html = '';
            if (Array.isArray(data)) {
                html = '<ul>';
                for (let item of data) {
                    html+=`<li>${item}</li>`;
                }
                html += '</ul>';
            } else {
                html = `<p>${data}</p>`;
            }

            this.modalContent.innerHTML = html;
        },

        getDataFromInput: function() {
            this.paramData = document.querySelector('#param').value;

            if (this.paramData.indexOf(',') !== -1) {
                this.paramData = this.paramData.split(',');
            } 
        },

        executeFunc: function(func, param) {
            let result = func(param);
            this.changeOutputContent(result);
        },

        clear: function(){
            document.querySelector('#param').value = '',
            this.changeOutputContent('');
        }

        
    }

    modal.init();

    let btnHandler = document.querySelectorAll('nav .btn');
    btnHandler.forEach((btn) => {
        btn.addEventListener('click', function() {
            modal.open(this.getAttribute('data'));
        });
    });

}